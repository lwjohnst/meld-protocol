
The overarching goal of the MELD project is to better quantify and understand the
impact that various early life conditions have on adult metabolic capacity and the
subsequent risk for incident T2D in adulthood. 

From a public and preventative health perspective, 
the main research questions would be:

1. Are some metabolic characteristics affected more by early life conditions than others?
1. Knowing the above and knowing an adult individual's history during early life, 
could we intervene and target specific metabolic characteristics to lower the risk
for T2DM? {{DISCUSS}}
1. Do some early life conditions affect an adult individual's metabolic status and
risk for disease more than other conditions?

The main project objectives are to:

1. Quantify the specific population-level impact of exposure to early life
adverse conditions on overall adult metabolic capacity and on risk for
developing type 2 diabetes in persons residing in Denmark;
2. Examine how early life factors may causally influence and be mediated by
adult metabolic capacity and on consequent risk for type 2 diabetes;
3. Identify which adverse conditions during early life could be potentially
targeted to optimally and practically reduce population-level incidence of type
2 diabetes;
4. Identify what metabolic pathways could be targeted to manage or mitigate
risk for type 2 diabetes in those who have been exposed to early life adversity;
5. Document and describe the exact steps taken for processing, analyzing,
interpreting, and visualizing this type of data as a resource for future
studies, given that few other countries have the necessary infrastructure and
data like Denmark has to answer and thus replicate the above objectives.

Early life adverse conditions here being defined as:

1. Major familial events such as parental divorce, parental and/or
sibling death or sudden illness and/or injury.
2. Major personal events such as violent injury and/or acute illness.
3. Chronic familial conditions such as low income, parental education, or social
capital, spousal abuse, unemployment, alcohol and drug abuse, living in social
housing, smoking during childhood and pregnancy, parental and/or sibling
physical or mental illness, having only a single parent.
4. Chronic personal conditions such as illness or injury, not being breastfed,
physical, mental, or emotional abuse, premature birth, being in foster care.

Potential indicators of early life conditions being:

1. Birth weight or length
2. Adult height and absolute and relative leg length

Metabolic condition and disease outcomes being defined as:

1. Total cases of type 2 diabetes at any given age.
2. Earlier diagnosis of type 2 diabetes at any given age.
3. Indicators of metabolic capacity at any given age in adult life.

The objectives will be completed within the next 5 years and the analyses for the
research objectives (1-4) will be completed within the next 2.5 years, in the
order of which exposures have the most accessible and available data to the
least.
